package test.revolute;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Account {
    private String id;
    private String owner;
    private Currency currency;
    private BigDecimal value;
    private Set<String> pendingTransactions = new HashSet<>();

    public Account(String id, String owner, Currency currency) {
        this.id = id;
        this.owner = owner;
        this.currency = currency;
        this.value = BigDecimal.ZERO;
    }

    public Account(String id, String owner, Currency currency, BigDecimal value) {
        this.id = id;
        this.owner = owner;
        this.currency = currency;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public Currency getCurrency() {
        return currency;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public void withdraw(String transacctionId, BigDecimal amount) {
        if (this.value.compareTo(amount) < 0) {
            throw new NotEnoughMoneyException("Account " + id + " dosn't have enough money. ");
        }
        if (pendingTransactions.add(transacctionId)) {
            this.value = this.value.subtract(amount);
        }
    }

    public void revertWithdraw(String transacctionId, BigDecimal amount) {
        if (pendingTransactions.remove(transacctionId)) {
            this.value =this.value.add(amount);
        }
    }

    public void deposit(String transacctionId, BigDecimal amount) {
        if (pendingTransactions.add(transacctionId)) {
            this.value = this.value.add(amount);
        }
    }

    public void revertDeposit(String transacctionId, BigDecimal amount) {
        if (pendingTransactions.remove(transacctionId)) {
            this.value.subtract(amount);
        }
    }

    public Set<String> getPendingTransactions() {
        return pendingTransactions;
    }

    public void applyTransaction(String transacctionId) {
        pendingTransactions.remove(transacctionId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(id, account.id) &&
                Objects.equals(owner, account.owner) &&
                Objects.equals(currency, account.currency);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, owner, currency);
    }
}
