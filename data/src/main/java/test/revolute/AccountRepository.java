package test.revolute;

public interface AccountRepository {
    Account findById(String id);

    Account save(Account account);
}
