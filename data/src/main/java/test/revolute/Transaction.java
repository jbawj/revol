package test.revolute;

import java.math.BigDecimal;

public class Transaction {
    public enum STATE {INITIAL, PENDING, APPLIED, DONE, ABORTED}

    private String id;
    private String from;
    private String to;
    private BigDecimal withdrawAmount;
    private BigDecimal depositAmount;
    private STATE state;
    private long lastModified;

    public Transaction(String from, String to, BigDecimal withdrawAmount) {
        this.from = from;
        this.to = to;
        this.withdrawAmount = withdrawAmount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public BigDecimal getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(BigDecimal withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }

    public BigDecimal getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(BigDecimal depositAmount) {
        this.depositAmount = depositAmount;
    }

    public Transaction updateState(STATE state) {
        this.state = state;
        this.lastModified = System.currentTimeMillis();
        return this;
    }

    public STATE getState() {
        return state;
    }

    public long getLastModified() {
        return lastModified;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id='" + id + '\'' +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", withdrawAmount=" + withdrawAmount +
                ", depositAmount=" + depositAmount +
                ", state=" + state +
                ", lastModified=" + lastModified +
                '}';
    }
}
