package test.revolute;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.inject.Singleton;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Currency;

@Singleton
public class SimpleConverterService implements CurrencyConverterService {
    private Table<Currency, Currency, BigDecimal> map = HashBasedTable.create();

    MathContext mc = new MathContext(4);

    public SimpleConverterService() {
        map.put(Currency.getInstance("USD"), Currency.getInstance("EUR"), BigDecimal.valueOf(0.87));
        map.put(Currency.getInstance("EUR"), Currency.getInstance("USD"), BigDecimal.valueOf(1.23));
    }

    @Override
    public BigDecimal convert(Currency from, Currency to, BigDecimal amount) {
        if (from.getCurrencyCode().equals(to.getCurrencyCode())) {
            return amount;
        }

        BigDecimal coef = map.get(from, to);
        if (coef == null) {
            throw new NoConvertionParamsException("No convertion from " + from + " to " + to);
        }

        return amount.multiply(coef, mc);
    }
}
