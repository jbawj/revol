package test.revolute;

public class NoConvertionParamsException extends RuntimeException {
    public NoConvertionParamsException(String message) {
        super(message);
    }
}
