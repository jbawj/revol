package test.revolute;

import com.google.inject.Singleton;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Singleton
public class InMemoryTransactionRepository implements TransactionRepository {
    private ConcurrentMap<String, Transaction> map = new ConcurrentHashMap<>();

    @Override
    public Transaction save(Transaction transaction) {
        map.put(transaction.getId(), transaction);

        return transaction;
    }

    @Override
    public Transaction updateState(Transaction transaction, Transaction.STATE state) {
        transaction.updateState(state);
        return save(transaction);
    }

    @Override
    public Transaction store(Transaction transaction) {
        transaction.setId(UUID.randomUUID().toString());
        transaction.updateState(Transaction.STATE.INITIAL);
        if (map.putIfAbsent(transaction.getId(), transaction) != null) {
            throw new RuntimeException(transaction.getId() + " already exists in repository");
        }

        return transaction;
    }
}
