package test.revolute;

import com.google.inject.Singleton;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Singleton
public class InMemoryAccountRepository implements AccountRepository {
    private ConcurrentMap<String, Account> map = new ConcurrentHashMap<>();

    public Account findById(String id) {
        return map.get(id);
    }

    public Account save(Account account) {
        if (account.getId() == null) {
            account.setId(UUID.randomUUID().toString());
        }
        map.putIfAbsent(account.getId(), account);

        return account;
    }
}
