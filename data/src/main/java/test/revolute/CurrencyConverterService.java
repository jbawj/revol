package test.revolute;

import java.math.BigDecimal;
import java.util.Currency;

public interface CurrencyConverterService {
    BigDecimal convert(Currency from, Currency to, BigDecimal amoun);
}
