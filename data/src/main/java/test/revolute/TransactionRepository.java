package test.revolute;

public interface TransactionRepository {
    Transaction save(Transaction transaction);

    Transaction updateState(Transaction transaction, Transaction.STATE state);

    Transaction store(Transaction transaction);
}
