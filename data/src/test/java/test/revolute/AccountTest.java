package test.revolute;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.junit.Assert.*;

public class AccountTest {
    @Test
    public void testDeposit() {
        Account account = new Account("id", "owner", Currency.getInstance("USD"));
        account.setValue(BigDecimal.valueOf(10));

        account.deposit("tr1", BigDecimal.valueOf(2));

        assertEquals(12.0, account.getValue().doubleValue(), 0);
        assertTrue(account.getPendingTransactions().contains("tr1"));

        account.applyTransaction("tr1");

        assertFalse(account.getPendingTransactions().contains("tr1"));
    }

    @Test
    public void testWithdrawal() {
        Account account = new Account("id", "owner", Currency.getInstance("USD"));
        account.setValue(BigDecimal.valueOf(10));

        account.withdraw("tr1", BigDecimal.valueOf(2));

        assertEquals(8.0, account.getValue().doubleValue(), 0);
        assertTrue(account.getPendingTransactions().contains("tr1"));

        account.applyTransaction("tr1");

        assertFalse(account.getPendingTransactions().contains("tr1"));
    }

    @Test
    public void testWithdrawalOverLimits() {
        Account account = new Account("id", "owner", Currency.getInstance("USD"));
        account.setValue(BigDecimal.valueOf(10));

        try {
            account.withdraw("tr1", BigDecimal.valueOf(12));
            fail("NotEnoughMoneyException must be trown");
        } catch (NotEnoughMoneyException e) {
            // that's expected
        }
    }
}
