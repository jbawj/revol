package test.revolute;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class SimpleConverterServiceTest {

    @Test
    public void testSuccess() {
        SimpleConverterService converterService = new SimpleConverterService();

        BigDecimal convert1 = converterService.convert(Currency.getInstance("USD"), Currency.getInstance("USD"), BigDecimal.valueOf(10));
        assertEquals(10.0, convert1.doubleValue(), 0);

        convert1 = converterService.convert(Currency.getInstance("USD"), Currency.getInstance("EUR"), BigDecimal.ONE);
        assertEquals(0.87, convert1.doubleValue(), 0);

        convert1 = converterService.convert(Currency.getInstance("EUR"), Currency.getInstance("USD"), BigDecimal.ONE);
        assertEquals(1.23, convert1.doubleValue(), 0);

        try {
            converterService.convert(Currency.getInstance("RUB"), Currency.getInstance("GBP"), BigDecimal.ONE);
            fail("NoConvertionParamsException must be thrown");
        } catch (NoConvertionParamsException e) {
            // that;s ok
        }
    }
}
