package test.revolute.processing.actor.message;

import java.math.BigDecimal;

public class Converted {
    private final BigDecimal value;

    public Converted(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getValue() {
        return value;
    }
}
