package test.revolute.processing.actor.message;

import akka.actor.ActorRef;

public class SetAccounRef {
    private final String accountId;
    private final ActorRef actorRef;

    public SetAccounRef(String accountId, ActorRef actorRef) {
        this.accountId = accountId;
        this.actorRef = actorRef;
    }

    public String getAccountId() {
        return accountId;
    }

    public ActorRef getActorRef() {
        return actorRef;
    }
}
