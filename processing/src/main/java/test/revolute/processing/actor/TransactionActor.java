package test.revolute.processing.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.ReceiveBuilder;
import test.revolute.TransactionRepository;
import test.revolute.processing.actor.AccountActor.CommitTransaction;
import test.revolute.Transaction;
import test.revolute.processing.actor.message.Converted;
import test.revolute.processing.actor.message.Failed;

import java.math.BigDecimal;

import static test.revolute.Transaction.STATE.*;

public class TransactionActor extends AbstractActor {
    static public Props props(ActorRef accountSupervisor, ActorRef convertor, TransactionRepository transactionRepository) {
        return Props.create(TransactionActor.class, () -> new TransactionActor(accountSupervisor, convertor, transactionRepository));
    }

    public static class Done {}

    public static class Init {
        private final String fromAccount;
        private final String toAccount;
        private final BigDecimal amount;

        public Init(String fromAccount, String toAccount, BigDecimal amount) {
            this.fromAccount = fromAccount;
            this.toAccount = toAccount;
            this.amount = amount;
        }
    }

    public static class SetAccountRef {
        private final String accountId;
        private final ActorRef actorRef;

        public SetAccountRef(String accountId, ActorRef actorRef) {
            this.accountId = accountId;
            this.actorRef = actorRef;
        }

        public String getAccountId() {
            return accountId;
        }

        public ActorRef getActorRef() {
            return actorRef;
        }
    }

    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    private ActorRef accountSupervisor;
    private ActorRef convertor;
    private ActorRef fromAccount;
    private ActorRef toAccount;
    private Transaction transaction;

    private boolean fromCommitted = false;
    private boolean toCommitted = false;

    private final TransactionRepository transactionRepository;

    private AbstractActor.Receive init;
    private AbstractActor.Receive pendingWithdraw;
    private AbstractActor.Receive pendingDeposit;
    private AbstractActor.Receive awaitForRevert;
    private AbstractActor.Receive awaitForCommit;

    private TransactionActor(ActorRef accountSupervisor, ActorRef converter, TransactionRepository transactionRepository) {
        this.accountSupervisor = accountSupervisor;
        this.convertor = converter;
        this.transactionRepository = transactionRepository;

        init = createCommon(createInit());
        pendingWithdraw = createCommon(createPendingWithdrawalState());
        pendingDeposit = createCommon(createPendingDeposit());
        awaitForRevert = createCommon(createAwaitForRevert());
        awaitForCommit = createCommon(createAwaitForCommit());
    }

    @Override
    public Receive createReceive() {
        return init;
    }

    private ReceiveBuilder createPendingWithdrawalState() {
        return receiveBuilder()
                .match(Done.class, msg -> {
                    toAccount.tell(new AccountActor.InitDeposit(transaction.getId(), transaction.getDepositAmount()), getSelf());
                    getContext().become(pendingDeposit);
                })
                .match(Failed.class, msg -> {
                    log.error("Withdrawal failed: {}", transaction);
                    abortTransaction();
                });
    }

    private ReceiveBuilder createAwaitForRevert() {
        return receiveBuilder()
                .match(Done.class, msg -> {
                    log.info("Withdrawal reverted: {}", transaction);
                    abortTransaction();
                });
    }

    private ReceiveBuilder createPendingDeposit() {
        return receiveBuilder()
                .match(Done.class, msg -> {
                    transaction = transactionRepository.updateState(transaction, APPLIED);
                    fromAccount.tell(new CommitTransaction(transaction.getId()), getSelf());
                    toAccount.tell(new CommitTransaction(transaction.getId()), getSelf());
                    log.info("Transaction applied: {}", transaction);
                    getContext().become(awaitForCommit);
                })
                .match(Failed.class, msg -> {
                    fromAccount.tell(new AccountActor.RevertWithdraw(transaction.getId(), transaction.getWithdrawAmount()), getSelf());
                    transaction = transactionRepository.updateState(transaction, ABORTED);
                    log.error("Transaction aborted, revert message sent: {}", transaction);
                    getContext().become(awaitForRevert);
                });
    }

    private ReceiveBuilder createAwaitForCommit() {
        return receiveBuilder()
                .match(Done.class, msg -> {
                    if (getSender().equals(toAccount)) {
                        toCommitted = true;
                    } else if (getSender().equals(fromAccount)) {
                        fromCommitted = true;
                    }

                    if (toCommitted && fromCommitted) {
                        transactionRepository.updateState(transaction, DONE);
                        log.info("Transfer done, Transaction: {}", transaction);
                        getContext().stop(getSelf());
                    }
                })
                .match(Failed.class, msg -> {
                    log.error("Panic! Need manual fix");
                    getContext().stop(getSelf());
                });
    }

    private ReceiveBuilder createInit() {
        return receiveBuilder()
                .match(Init.class, msg -> {
                    this.transaction = new Transaction(msg.fromAccount, msg.toAccount, msg.amount);
                    convertor.tell(new ConverterActor.Convert(msg.fromAccount, msg.toAccount, msg.amount), getSelf());
                    log.info("Request for convertion sent: {}", transaction);
                })
                .match(Converted.class, msg -> {
                    transaction.setDepositAmount(msg.getValue());

                    accountSupervisor.tell(new AccountSupervisorActor.GetAccountRef(transaction.getFrom()), getSelf());
                    accountSupervisor.tell(new AccountSupervisorActor.GetAccountRef(transaction.getTo()), getSelf());
                    log.info("Converted amount received: {}", transaction);
                })
                .match(Failed.class, msg -> {
                    log.error("Failed initialization: {}", transaction);
                    abortTransaction();
                })
                .match(SetAccountRef.class, msg -> {
                    if (msg.accountId.equals(transaction.getFrom())) {
                        fromAccount = msg.actorRef;
                    } else if (msg.accountId.equals(transaction.getTo())) {
                        toAccount = msg.actorRef;
                    }

                    if (fromAccount != null && toAccount != null) {
                        runTransaction();
                    }
                });
    }
    private AbstractActor.Receive createCommon(ReceiveBuilder builder) {
        return builder
                .matchAny(msg -> log.error("Unhadled message type", msg))
                .build();
    }

    private void runTransaction() {
        transaction = transactionRepository.store(transaction);
        transaction = transactionRepository.updateState(transaction, PENDING);
        fromAccount.tell(new AccountActor.InitWithdraw(transaction.getId(), transaction.getWithdrawAmount()), getSelf());

        getContext().become(pendingWithdraw);

        log.info("Transaction started: {}", transaction);
    }

    private void abortTransaction() {
        transactionRepository.updateState(transaction, Transaction.STATE.ABORTED);
        getContext().stop(getSelf());
    }
}
