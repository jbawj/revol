package test.revolute.processing.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import scala.Option;
import test.revolute.Account;
import test.revolute.AccountRepository;
import test.revolute.processing.actor.message.Failed;

public class AccountSupervisorActor extends AbstractActor {
    static public Props props(AccountRepository accountRepository) {
        return Props.create(AccountSupervisorActor.class, () -> new AccountSupervisorActor(accountRepository));
    }

    public static class GetAccountRef {
        private final String accountId;

        public GetAccountRef(String accountId) {
            this.accountId = accountId;
        }
    }

    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    private final AccountRepository accountRepository;

    public AccountSupervisorActor(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(GetAccountRef.class, msg -> {
                    try {
                        Account account = accountRepository.findById(msg.accountId);
                        if (account == null) {
                            getSender().tell(new Failed(), getSelf());
                        } else {
                            Option<ActorRef> child = getContext().child(account.getId());
                            if (child.isDefined()) {
                                getSender().tell(new TransactionActor.SetAccountRef(account.getId(), child.get()), getSelf());
                            } else {
                                ActorRef actorRef = getContext().actorOf(AccountActor.props(account), account.getId());
                                getSender().tell(new TransactionActor.SetAccountRef(account.getId(), actorRef), getSelf());
                            }
                        }
                    } catch (Exception e) {
                        log.error(e, e.getMessage());
                        getSender().tell(new Failed(), getSelf());
                    }

                })
                .build();
    }
}
