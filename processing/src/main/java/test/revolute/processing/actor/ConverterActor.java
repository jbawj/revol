package test.revolute.processing.actor;

import akka.actor.AbstractActor;
import akka.actor.Props;
import test.revolute.Account;
import test.revolute.AccountRepository;
import test.revolute.CurrencyConverterService;
import test.revolute.NoConvertionParamsException;
import test.revolute.processing.actor.message.Converted;
import test.revolute.processing.actor.message.Failed;

import java.math.BigDecimal;

public class ConverterActor extends AbstractActor {
    static public Props props(CurrencyConverterService converterService, AccountRepository accountRepository) {
        return Props.create(ConverterActor.class, () -> new ConverterActor(converterService, accountRepository));
    }

    public static class Convert {
        private final String fromAccount;
        private final String toAccount;
        private final BigDecimal amount;

        public Convert(String fromAccount, String toAccount, BigDecimal amount) {
            this.fromAccount = fromAccount;
            this.toAccount = toAccount;
            this.amount = amount;
        }
    }
    private final CurrencyConverterService converterService;
    private final AccountRepository accountRepository;

    private ConverterActor(CurrencyConverterService converterService, AccountRepository accountRepository) {
        this.converterService = converterService;
        this.accountRepository = accountRepository;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(Convert.class, msg -> {
                    try {
                        Account fromAccount = accountRepository.findById(msg.fromAccount);
                        if (fromAccount == null) {
                            getSender().tell(new Failed(), getSelf());
                            return;
                        }
                        Account toAccount = accountRepository.findById(msg.toAccount);
                        if (toAccount == null) {
                            getSender().tell(new Failed(), getSelf());
                            return;
                        }
                        BigDecimal convertedAmount = converterService.convert(fromAccount.getCurrency(), toAccount.getCurrency(), msg.amount);
                        getSender().tell(new Converted(convertedAmount), getSelf());
                    } catch (NoConvertionParamsException e) {
                        getSender().tell(new Failed(), getSelf());
                    }
                })
                .build();
    }
}
