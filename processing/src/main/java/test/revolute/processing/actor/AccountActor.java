package test.revolute.processing.actor;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import test.revolute.Account;
import test.revolute.processing.actor.message.Failed;

import java.math.BigDecimal;

public class AccountActor extends AbstractActor {
    static public Props props(Account account) {
        return Props.create(AccountActor.class, () -> new AccountActor(account));
    }

    public static class InitWithdraw {
        private final String transactionId;
        private final BigDecimal amount;

        public InitWithdraw(String transactionId, BigDecimal amount) {
            this.transactionId = transactionId;
            this.amount = amount;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public BigDecimal getAmount() {
            return amount;
        }
    }

    public static class RevertWithdraw {
        private final String transacctionId;
        private final BigDecimal amount;

        public RevertWithdraw(String transacctionId, BigDecimal amount) {
            this.transacctionId = transacctionId;
            this.amount = amount;
        }

        public String getTransacctionId() {
            return transacctionId;
        }

        public BigDecimal getAmount() {
            return amount;
        }
    }

    public static class InitDeposit {
        private final String transacctionId;
        private final BigDecimal amount;

        public InitDeposit(String transacctionId, BigDecimal amount) {
            this.transacctionId = transacctionId;
            this.amount = amount;
        }

        public String getTransacctionId() {
            return transacctionId;
        }

        public BigDecimal getAmount() {
            return amount;
        }
    }

    public static class CommitTransaction {
        private final String transactionId;

        public CommitTransaction(String transactionId) {
            this.transactionId = transactionId;
        }
    }

    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    private final Account account;

    private AccountActor(Account account) {
        this.account = account;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(InitWithdraw.class, msg -> {
                    if (account.getValue().compareTo(msg.amount) < 0) {
                        getSender().tell(new Failed(), getSelf());
                    } else {
                        try {
                            account.withdraw(msg.transactionId, msg.amount);
                            getSender().tell(new TransactionActor.Done(), getSelf());
                        } catch (Exception e) {
                            log.error(e, e.getMessage());
                            getSender().tell(new Failed(), getSelf());
                        }

                    }
                })
                .match(InitDeposit.class, msg -> {
                    try {
                        account.deposit(msg.transacctionId, msg.amount);
                        getSender().tell(new TransactionActor.Done(), getSelf());
                    } catch (Exception e) {
                        log.error(e, e.getMessage());
                        getSender().tell(new Failed(), getSelf());
                    }

                })
                .match(RevertWithdraw.class, msg -> {
                    account.revertWithdraw(msg.transacctionId, msg.amount);
                    getSender().tell(new TransactionActor.Done(), getSelf());
                })
                .match(CommitTransaction.class, msg -> {
                    try {
                        account.applyTransaction(msg.transactionId);
                        getSender().tell(new TransactionActor.Done(), getSelf());
                    } catch (Exception e) {
                        getSender().tell(new Failed(), getSelf());
                    }
                })
                .matchAny(o -> log.info("received unknown message"))
                .build();
    }
}
