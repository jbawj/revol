package test.revolute.processing.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.Terminated;
import akka.routing.ActorRefRoutee;
import akka.routing.RoundRobinRoutingLogic;
import akka.routing.Routee;
import akka.routing.Router;
import test.revolute.AccountRepository;
import test.revolute.CurrencyConverterService;

import java.util.ArrayList;
import java.util.List;

public class MasterConverterActor extends AbstractActor {
    static public Props props(CurrencyConverterService converterService, AccountRepository accountRepository) {
        return Props.create(MasterConverterActor.class, () -> new MasterConverterActor(converterService, accountRepository));
    }

    Router router;
    private final CurrencyConverterService converterService;
    private final AccountRepository accountRepository;

    private MasterConverterActor(CurrencyConverterService converterService, AccountRepository accountRepository) {
        this.converterService = converterService;
        this.accountRepository = accountRepository;

        List<Routee> routees = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            ActorRef r = getContext().actorOf(ConverterActor.props(converterService, accountRepository));
            getContext().watch(r);
            routees.add(new ActorRefRoutee(r));
        }
        router = new Router(new RoundRobinRoutingLogic(), routees);

    }
    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(ConverterActor.Convert.class, msg -> {
                    router.route(msg, getSender());
                })
                .match(Terminated.class, message -> {
                    router = router.removeRoutee(message.actor());
                    ActorRef r = getContext().actorOf(ConverterActor.props(converterService, accountRepository));
                    getContext().watch(r);
                    router = router.addRoutee(new ActorRefRoutee(r));
                })
                .build();
    }
}
