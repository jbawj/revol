package test.revolute.processing;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;
import org.apache.commons.lang3.mutable.MutableObject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import test.revolute.*;
import test.revolute.processing.actor.*;
import test.revolute.processing.actor.message.Converted;
import test.revolute.processing.actor.message.Failed;

import java.math.BigDecimal;
import java.util.Currency;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class AkkaTest {
    static ActorSystem system;

    @BeforeClass
    public static void setup() {
        system = ActorSystem.create();
    }

    @AfterClass
    public static void teardown() {
        TestKit.shutdownActorSystem(system);
        system = null;
    }

    @Test
    public void testSupervisor() {
        new TestKit(system) {{
            Account account1 = new Account("id1", "owner1", Currency.getInstance("USD"));
            account1.setValue(BigDecimal.valueOf(100.0));

            AccountRepository accountRepository = Mockito.mock(AccountRepository.class);
            when(accountRepository.findById("id1")).thenReturn(account1);

            final ActorRef accountSupervisor = system.actorOf(AccountSupervisorActor.props(accountRepository));
            accountSupervisor.tell(new AccountSupervisorActor.GetAccountRef(account1.getId()), getRef());
            TransactionActor.SetAccountRef setAccountRef = expectMsgClass(TransactionActor.SetAccountRef.class);

            accountSupervisor.tell(new AccountSupervisorActor.GetAccountRef(account1.getId()), getRef());
            TransactionActor.SetAccountRef setAccountRef2 = expectMsgClass(TransactionActor.SetAccountRef.class);

            assertEquals(setAccountRef.getActorRef(), setAccountRef2.getActorRef());

        }};
    }

    @Test
    public void testSupervisorNoAccount() {
        new TestKit(system) {{
            Account account1 = new Account("id1", "owner1", Currency.getInstance("USD"));
            account1.setValue(BigDecimal.valueOf(100.0));

            AccountRepository accountRepository = Mockito.mock(AccountRepository.class);
            when(accountRepository.findById("id1")).thenReturn(account1);

            final ActorRef accountSupervisor = system.actorOf(AccountSupervisorActor.props(accountRepository));
            accountSupervisor.tell(new AccountSupervisorActor.GetAccountRef("noacc"), getRef());
            Failed setAccountRef = expectMsgClass(Failed.class);

        }};
    }

    @Test
    public void testTransfer() {
        new TestKit(system) {{
            Account account1 = new Account("id1", "owner1", Currency.getInstance("USD"));
            account1.setValue(BigDecimal.valueOf(100.0));
            Account account2 = new Account("id2", "owner2", Currency.getInstance("USD"));
            account2.setValue(BigDecimal.valueOf(100.0));

            AccountRepository accountRepository = Mockito.mock(AccountRepository.class);
            when(accountRepository.findById("id1")).thenReturn(account1);
            when(accountRepository.findById("id2")).thenReturn(account2);

            final ActorRef accountSupervisor = system.actorOf(AccountSupervisorActor.props(accountRepository));
            final ActorRef converter = system.actorOf(MasterConverterActor.props(createConverterService(), accountRepository));
            Props props = TransactionActor.props(accountSupervisor, converter, createTransactionRepository());
            final ActorRef transferRef = system.actorOf(props);
            transferRef.tell(new TransactionActor.Init(account1.getId(), account2.getId(), BigDecimal.valueOf(10)), getRef());

            watch(transferRef);
            expectTerminated(transferRef);
            assertEquals(90.0, account1.getValue().doubleValue(), 0);
            assertEquals(110.0, account2.getValue().doubleValue(), 0);
        }};
    }

    @Test
    public void testTransactionState() {
        new TestKit(system) {{
            Account account1 = new Account("id1", "owner1", Currency.getInstance("USD"));
            account1.setValue(BigDecimal.valueOf(100.0));
            Account account2 = new Account("id2", "owner2", Currency.getInstance("USD"));
            account2.setValue(BigDecimal.valueOf(100.0));

            AccountRepository accountRepository = Mockito.mock(AccountRepository.class);
            when(accountRepository.findById("id1")).thenReturn(account1);
            when(accountRepository.findById("id2")).thenReturn(account2);

            MutableObject<Transaction> transaction = new MutableObject<>();
            TransactionRepository transactionRepository = createTransactionRepository();
            when(transactionRepository.store(any())).thenAnswer(invocation -> {
                Transaction tr = (Transaction) invocation.getArguments()[0];
                transaction.setValue(tr);

                return tr;
            });

            double fromAmount = 10;
            double convertedAmount = 12;
            Props props = TransactionActor.props(getRef(), getRef(), transactionRepository);
            final ActorRef transferRef = system.actorOf(props);
            transferRef.tell(new TransactionActor.Init(account1.getId(), account2.getId(), BigDecimal.valueOf(fromAmount)), getRef());

            ConverterActor.Convert requestForConvert = expectMsgClass(ConverterActor.Convert.class);

            transferRef.tell(new Converted(BigDecimal.valueOf(convertedAmount)), getRef());
            AccountSupervisorActor.GetAccountRef getAccount1 = expectMsgClass(AccountSupervisorActor.GetAccountRef.class);
            AccountSupervisorActor.GetAccountRef getAccount2 = expectMsgClass(AccountSupervisorActor.GetAccountRef.class);

            transferRef.tell(new TransactionActor.SetAccountRef(account1.getId(), getRef()), getRef());
            transferRef.tell(new TransactionActor.SetAccountRef(account2.getId(), getRef()), getRef());

            AccountActor.InitWithdraw initWithdraw = expectMsgClass(AccountActor.InitWithdraw.class);
            assertEquals(Transaction.STATE.PENDING, transaction.getValue().getState());
            assertEquals(fromAmount, initWithdraw.getAmount().doubleValue(), 0);
            transferRef.tell(new TransactionActor.Done(), getRef());

            AccountActor.InitDeposit initDeposit = expectMsgClass(AccountActor.InitDeposit.class);
            assertEquals(Transaction.STATE.PENDING, transaction.getValue().getState());
            assertEquals(convertedAmount, initDeposit.getAmount().doubleValue(), 0);
            transferRef.tell(new TransactionActor.Done(), getRef());

            expectMsgClass(AccountActor.CommitTransaction.class);
            expectMsgClass(AccountActor.CommitTransaction.class);
            transferRef.tell(new TransactionActor.Done(), getRef());
            transferRef.tell(new TransactionActor.Done(), getRef());
            assertEquals(Transaction.STATE.APPLIED, transaction.getValue().getState());

        }};
    }

    @Test
    public void testTransferFailedLimitExceeded() {
        new TestKit(system) {{
            Account account1 = new Account("id1", "owner1", Currency.getInstance("USD"));
            account1.setValue(BigDecimal.valueOf(100.0));
            Account account2 = new Account("id2", "owner2", Currency.getInstance("USD"));
            account2.setValue(BigDecimal.valueOf(100.0));

            AccountRepository accountRepository = Mockito.mock(AccountRepository.class);
            when(accountRepository.findById("id1")).thenReturn(account1);
            when(accountRepository.findById("id2")).thenReturn(account2);

            CurrencyConverterService converterService = Mockito.mock(CurrencyConverterService.class);

            final ActorRef accountSupervisor = system.actorOf(AccountSupervisorActor.props(accountRepository));
            final ActorRef converter = system.actorOf(ConverterActor.props(converterService, accountRepository));
            Props props = TransactionActor.props(accountSupervisor, converter, createTransactionRepository());
            final ActorRef transferRef = system.actorOf(props);
            transferRef.tell(new TransactionActor.Init(account1.getId(), account2.getId(), BigDecimal.valueOf(110)), getRef());

            watch(transferRef);
            expectTerminated(transferRef);
            assertEquals(100.0, account1.getValue().doubleValue(), 0);
            assertEquals(100.0, account2.getValue().doubleValue(), 0);
        }};
    }

    @Test
    public void testTransferFailedNoAccount() {
        new TestKit(system) {{
            AccountRepository accountRepository = Mockito.mock(AccountRepository.class);

            CurrencyConverterService converterService = Mockito.mock(CurrencyConverterService.class);

            final ActorRef accountSupervisor = system.actorOf(AccountSupervisorActor.props(accountRepository));
            final ActorRef converter = system.actorOf(ConverterActor.props(converterService, accountRepository));
            Props props = TransactionActor.props(accountSupervisor, converter, createTransactionRepository());
            final ActorRef transferRef = system.actorOf(props);
            transferRef.tell(new TransactionActor.Init("no1", "no2", BigDecimal.valueOf(110)), getRef());

            watch(transferRef);
            expectTerminated(transferRef);
        }};
    }

    private TransactionRepository createTransactionRepository() {
        TransactionRepository transactionRepository = Mockito.mock(TransactionRepository.class);
        when(transactionRepository.store(any(Transaction.class))).thenAnswer(invocation -> invocation.getArguments()[0]);
        when(transactionRepository.updateState(any(Transaction.class), any(Transaction.STATE.class))).thenAnswer(invocation -> {
            Transaction transaction = (Transaction) invocation.getArguments()[0];
            Transaction.STATE state = (Transaction.STATE) invocation.getArguments()[1];
            transaction.updateState(state);

            return transaction;
        });

        return transactionRepository;
    }

    private CurrencyConverterService createConverterService() {
        CurrencyConverterService converterService = Mockito.mock(CurrencyConverterService.class);
        when(converterService.convert(any(Currency.class), any(Currency.class), any(BigDecimal.class))).thenAnswer(invocation -> {
            Currency from = (Currency) invocation.getArguments()[0];
            Currency to = (Currency) invocation.getArguments()[1];
            BigDecimal amount = (BigDecimal) invocation.getArguments()[2];

            if (from.getCurrencyCode().equals(to.getCurrencyCode())) {
                return amount;
            } else {
                return amount.multiply(BigDecimal.valueOf(2));
            }
        });

        return converterService;
    }
}
