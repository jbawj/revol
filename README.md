
**To quickstart use the following command**
```bash
 mvn -pl transfer exec:exec
``` 
It will start http service on 8080 port

##API
*  To get an account info by id
```text
http://<hosname>:<port>/get/<id>
```
*  To create an account (all params are required)
```text
http://<hosname>:<port>/create?id=<id>&cur=<cur>&val=<val>
```
    id - account id
    cur - Currency(USD, EUR, ...).
    val - an initial amount of money
*  To transfer money from one account to another
```text
http://<hosname>:<port>/transfer?from=<from>&to=<to>&amount=<amount>
```
    from - source account id
    to - destination account id
    amount - an amount to be transfered

For now the convertion from USD to EUR and vise versa is supported. Convertion for other currencies is impossible.
