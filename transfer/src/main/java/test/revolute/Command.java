package test.revolute;

public enum Command {
    GET, CREATE, TRANSFER
}
