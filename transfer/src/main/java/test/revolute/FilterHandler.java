package test.revolute;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import io.netty.handler.codec.http.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.regex.*;

import static test.revolute.Constants.*;

public class FilterHandler extends MessageToMessageDecoder<FullHttpRequest> {
    private static final String PREFIX_URL_GET = "/get";
    private static final String PREFIX_URL_CREATE = "/create";
    private static final String PREFIX_URL_TRANSFER = "/transfer";
    private static final Pattern URL_PATTERN_TRANSFER = Pattern.compile(PREFIX_URL_TRANSFER + "(?:\\?from=(\\w+)&to=(\\w+)&amount=(\\d+))");
    private static final Pattern URL_PATTERN_CREATE = Pattern.compile(PREFIX_URL_CREATE + "(?:\\?id=(\\w+)&cur=(\\w+)&val=(\\w+))");

    private Logger log = LoggerFactory.getLogger(getClass().getName());

    @Override
    protected void decode(ChannelHandlerContext ctx, FullHttpRequest request, List<Object> out) throws Exception {
        String url = request.uri();
        url = url == null ? "" : url.toLowerCase();
        if (!url.startsWith(PREFIX_URL_TRANSFER) && !url.startsWith(PREFIX_URL_GET) && !url.startsWith(PREFIX_URL_CREATE)) {
            log.error("Wrong URI: {}", url);
            HttpServer.sendError(ctx, "Wrong URI", HttpResponseStatus.NOT_FOUND);
            return;
        }

        if (url.startsWith(PREFIX_URL_GET)) {
            int index = url.lastIndexOf('/');
            if (index == -1) {
                log.error("Cant extract account id: {}", url);
                HttpServer.sendError(ctx, "Wrong request", HttpResponseStatus.BAD_REQUEST);
                return;
            }

            request.headers().add(PARAM_COMMAND, Command.GET);
            request.headers().add(PARAM_ACCOUNT, url.substring(index + 1));


        } else if (url.startsWith(PREFIX_URL_CREATE)) {
            Matcher matcher = URL_PATTERN_CREATE.matcher(url);
            if (!matcher.find()) {
                HttpServer.sendError(ctx, "Wrong parameters", HttpResponseStatus.BAD_REQUEST);
                return;
            }

            request.headers().add(PARAM_COMMAND, Command.CREATE);
            request.headers().add(PARAM_ACCOUNT, matcher.group(1));
            request.headers().add(PARAM_CURRENCY, matcher.group(2));
            request.headers().add(PARAM_VALUE, matcher.group(3));

        } else if (url.startsWith(PREFIX_URL_TRANSFER)) {
            Matcher matcher = URL_PATTERN_TRANSFER.matcher(url);
            if (!matcher.find()) {
                HttpServer.sendError(ctx, "Wrong parameters", HttpResponseStatus.BAD_REQUEST);
                return;
            }

            request.headers().add(PARAM_COMMAND, Command.TRANSFER);
            request.headers().add(PARAM_ACCOUNT, matcher.group(1));
            request.headers().add(PARAM_ACCOUNT_TO, matcher.group(2));
            request.headers().add(PARAM_VALUE, matcher.group(3));
        }

        out.add(request);
        request.retain();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        HttpServer.sendError(ctx, "Internal error:" + cause.getMessage(), HttpResponseStatus.INTERNAL_SERVER_ERROR);
    }
}
