package test.revolute;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.google.common.base.Charsets;
import com.google.inject.Guice;
import com.google.inject.Injector;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.*;
import test.revolute.processing.actor.AccountSupervisorActor;
import test.revolute.processing.actor.MasterConverterActor;

public class HttpServer {
    private int port;
    private AccountRepository accountRepository;
    private TransactionRepository transactionRepository;
    private CurrencyConverterService converterService;

    public HttpServer(int port, AccountRepository accountRepository, TransactionRepository transactionRepository, CurrencyConverterService converterService) {
        this.port = port;
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
        this.converterService = converterService;
    }

    public void run() throws InterruptedException {

        EventLoopGroup workerGroup = new NioEventLoopGroup();
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        ChannelFuture channelFuture = null;

        final ActorSystem system = ActorSystem.create("bank");

        try {
            ActorRef accountSupervisor = system.actorOf(AccountSupervisorActor.props(accountRepository));
            ActorRef converter = system.actorOf(MasterConverterActor.props(converterService, accountRepository));

            ServerBootstrap server = new ServerBootstrap()
                    .group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<Channel>() {
                        @Override
                        public void initChannel(Channel ch) throws Exception {
                            ch.pipeline()
                                    .addLast(new HttpServerCodec())
                                    .addLast(new HttpObjectAggregator(Integer.MAX_VALUE))
                                    .addLast(new FilterHandler())
                                    .addLast(new WorkerHandler(accountRepository, transactionRepository, system, accountSupervisor, converter));
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 500)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);

            channelFuture = server.bind("localhost", port).sync();

            channelFuture.channel().closeFuture().sync();

        } finally {
            system.terminate();
            workerGroup.shutdownGracefully();
            if (channelFuture != null) channelFuture.channel().close().awaitUninterruptibly();
        }
    }
    public static void sendError(ChannelHandlerContext ctx, String errorMessage, HttpResponseStatus status) {
        ByteBuf content = Unpooled.copiedBuffer(errorMessage, Charsets.UTF_8);
        FullHttpResponse response =
                new DefaultFullHttpResponse(
                        HttpVersion.HTTP_1_1,
                        status,
                        content);

        response.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/plain");
        response.headers().set(HttpHeaderNames.CONTENT_LENGTH, response.content().readableBytes());
        response.headers().set(HttpHeaderNames.ACCEPT_CHARSET, Charsets.UTF_8.name());

        ChannelFuture channelFuture = ctx.writeAndFlush(response);
        channelFuture.addListener(ChannelFutureListener.CLOSE);
    }

    public static void main(String[] args) throws Exception {
        Injector injector = Guice.createInjector(new DependencyModule());
        AccountRepository accountRepository = injector.getInstance(AccountRepository.class);
        TransactionRepository transactionRepository = injector.getInstance(TransactionRepository.class);
        CurrencyConverterService converterService = injector.getInstance(CurrencyConverterService.class);
        HttpServer server = new HttpServer(8080, accountRepository, transactionRepository, converterService);
        server.run();
    }
}
