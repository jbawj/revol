package test.revolute;

public interface Constants {
    String PARAM_COMMAND = "command";
    String PARAM_ACCOUNT = "account";
    String PARAM_ACCOUNT_TO = "accountTo";
    String PARAM_CURRENCY = "currency";
    String PARAM_VALUE = "value";
}