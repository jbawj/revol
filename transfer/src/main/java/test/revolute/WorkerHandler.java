package test.revolute;


import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.google.common.base.Charsets;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.revolute.processing.actor.TransactionActor;

import java.math.BigDecimal;
import java.util.Currency;

public class WorkerHandler extends ChannelInboundHandlerAdapter {
    private Logger log = LoggerFactory.getLogger(getClass().getName());

    private AccountRepository accountRepository;
    private TransactionRepository transactionRepository;
    private ActorSystem system;
    private ActorRef accountSupervisor;
    private ActorRef converter;

    public WorkerHandler(AccountRepository accountRepository, TransactionRepository transactionRepository, ActorSystem system, ActorRef accountSupervisor, ActorRef converter) {
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
        this.system = system;
        this.accountSupervisor = accountSupervisor;
        this.converter = converter;

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        HttpServer.sendError(ctx, "Internal error:" + cause.getMessage(), HttpResponseStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object obj) throws Exception {
        FullHttpRequest request = (FullHttpRequest) obj;

        Command command = Command.valueOf(request.headers().get(Constants.PARAM_COMMAND));

        ByteBuf content = null;
        switch (command) {
            case GET:
                content = processGet(request.headers().get(Constants.PARAM_ACCOUNT));
                break;
            case CREATE:
                String id = request.headers().get(Constants.PARAM_ACCOUNT);
                String currency = request.headers().get(Constants.PARAM_CURRENCY);
                String value = request.headers().get(Constants.PARAM_VALUE);

                content = processCreate(id, "", Currency.getInstance(currency.toUpperCase()), new BigDecimal(value));
                break;
            case TRANSFER:
                String from = request.headers().get(Constants.PARAM_ACCOUNT);
                String to = request.headers().get(Constants.PARAM_ACCOUNT_TO);
                String amount = request.headers().get(Constants.PARAM_VALUE);

                content = processTRansfer(from, to, new BigDecimal(amount));
                break;
        }
        FullHttpResponse response =
                new DefaultFullHttpResponse(
                        HttpVersion.HTTP_1_1,
                        HttpResponseStatus.OK,
                        content);

        response.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/plain");
        response.headers().set(HttpHeaderNames.CONTENT_LENGTH, response.content().readableBytes());
        response.headers().set(HttpHeaderNames.ACCEPT_CHARSET, Charsets.UTF_8.name());

        ChannelFuture channelFuture = ctx.writeAndFlush(response);
        channelFuture.addListener(ChannelFutureListener.CLOSE);

        request.release();
    }

    private ByteBuf processGet(String accountId) {
        Account account = accountRepository.findById(accountId);
        if (account == null) {
            log.error("Account not found, id: {}", account);
            throw new RuntimeException("Account not found");

        }

        ByteBuf content = Unpooled.copiedBuffer(accountJSON(account), Charsets.UTF_8);

        return content;
    }

    private ByteBuf processCreate(String id, String owner, Currency currency, BigDecimal value) {
        Account account = new Account(id, owner, currency, value);
        account = accountRepository.save(account);

        ByteBuf content = Unpooled.copiedBuffer(accountJSON(account), Charsets.UTF_8);

        return content;
    }

    private ByteBuf processTRansfer(String from, String to, BigDecimal amount) {
        ActorRef actorRef = system.actorOf(TransactionActor.props(accountSupervisor, converter, transactionRepository));
        actorRef.tell(new TransactionActor.Init(from, to, amount), ActorRef.noSender());
        StringBuilder sb = new StringBuilder();
        sb.append("{status: \"Success\"}");
        return Unpooled.copiedBuffer(sb.toString(), Charsets.UTF_8);
    }

    private String accountJSON(Account account) {
        StringBuilder sb = new StringBuilder();
        sb.append('{')
                .append("id: \"").append(account.getId()).append('"').append(',')
                .append("amount: ").append(account.getValue().doubleValue()).append(',')
                .append("currency: \"").append(account.getCurrency().getCurrencyCode()).append('"')
                .append("}");
        return sb.toString();
    }
}
