package test.revolute;

import com.google.inject.AbstractModule;
import test.revolute.*;

public class DependencyModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(AccountRepository.class).to(InMemoryAccountRepository.class);
        bind(TransactionRepository.class).to(InMemoryTransactionRepository.class);
        bind(CurrencyConverterService.class).to(SimpleConverterService.class);
    }
}
