import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

public class StressTest {

    public static void main(String[] args) throws IOException, SAXException {
        int num =  10;
        CountDownLatch latch = new CountDownLatch(num);
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    latch.await();
                    long startTimestamp = System.currentTimeMillis();
                    WebConversation wc = new WebConversation();
                    WebRequest req = new GetMethodWebRequest( "http://localhost:8080/transfer?from=321&to=123&amount=10" );
                    WebResponse resp = wc.getResponse( req );
                    long stopTimestamp = System.currentTimeMillis();
                    System.out.println("start=" + startTimestamp + " " + new String(resp.getBytes()) + " stop=" + stopTimestamp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        for (int i = 0; i < num; i++) {
            new Thread(run).start();
            latch.countDown();
        }
    }
}
